import React, { Component } from "react";
import RenderGlass from "./RenderGlass";
export default class AllLayout extends Component {
  render() {
    return (
      <div className="container">
        <RenderGlass />
      </div>
    );
  }
}
