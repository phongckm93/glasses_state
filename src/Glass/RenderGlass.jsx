import React, { Component } from "react";
export default class RenderGlass extends Component {
  styles = { height: 300, width: 300 };
  handleImg = (url, name, desc, price) => {
    this.setState({
      imageUrl: url,
      name: "Name:" + name,
      description: desc,
      price: " Price:" + price + "$",
    });

    let a = document.querySelectorAll(".image_glass_show img");
    a.forEach(element => {
      element.style.backgroundColor = "";
    });
    document.getElementById(name).style.backgroundColor = "red";
  };
  state = {
    glassArr: [
      {
        id: 1,
        price: 30,
        name: "GUCCI G8850U",
        url: "./glassesImage/v1.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 2,
        price: 50,
        name: "GUCCI G8759H",
        url: "./glassesImage/v2.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 3,
        price: 30,
        name: "DIOR D6700HQ",
        url: "./glassesImage/v3.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 4,
        price: 70,
        name: "DIOR D6005U",
        url: "./glassesImage/v4.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 5,
        price: 40,
        name: "PRADA P8750",
        url: "./glassesImage/v5.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 6,
        price: 60,
        name: "PRADA P9700",
        url: "./glassesImage/v6.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 7,
        price: 80,
        name: "FENDI F8750",
        url: "./glassesImage/v7.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 8,
        price: 100,
        name: "FENDI F8500",
        url: "./glassesImage/v8.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 9,
        price: 60,
        name: "FENDI F4300",
        url: "./glassesImage/v9.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
    ],
    imageUrl: "",
    name: "",
    description: "",
    price: "",
  };

  render() {
    const position_img = {
      position: "absolute",
      top: "32%",
      left: "50%",
      transform: "translate(-50%,-50%)",
      width: "60%",

    };
    return (
      <div>
        <div className="row justify-content-around py-3">
          <section className="col-3 p-0">
            <img
              className="rounded-lg"
              src='./glassesImage/model.jpg' alt="model"
              style={{ width: "100%", height: 400 }}
            />
          </section>
          <section className="col-3 p-0">
            <img
              className="rounded-lg"
              src='./glassesImage/model.jpg' alt="model"
              style={{ width: "100%", height: 400 }}
            />
            <img src={this.state.imageUrl} style={position_img} alt="" />
            <p
              className="text-left mb-0 py-2 bg-light"
              style={{
                position: "absolute",
                bottom: 0,
              }}
            >
              <span className="font-weight-bold">{this.state.name}</span>
              <br />
              <span className="font-weight-bold text-danger">
                {this.state.price}
              </span>
              <br />
              <span className="font-italic">{this.state.description}</span>
            </p>
          </section>
        </div>
        <div className="image_glass_show m-auto bg-white col-10 p-3 rounded-lg">
          {this.state.glassArr.map((item, i) => {
            return (
              <img id={item.name}
                key={i}
                onClick={() => {
                  this.handleImg(item.url, item.name, item.desc, item.price);
                }}
                className="col-3 m-1"
                src={item.url}
                alt="" style={{ cursor: "pointer" }}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
