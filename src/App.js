
import './App.css';
import AllLayout from './Glass/AllLayout';
function App() {
  const backgroundFashion = {
    backgroundImage: 'url(./glassesImage/bg.webp)', height: 'auto', backgroundSize: 'cover',
  }
  return (
    <div className="App" style={backgroundFashion}>
      <AllLayout />
    </div>
  );
}

export default App




